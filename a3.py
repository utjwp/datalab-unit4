import os
import pandas as pd
from sklearn.preprocessing import StandardScaler, LabelEncoder
from sklearn.cluster import DBSCAN
from sklearn.cluster import KMeans
from sklearn.decomposition import PCA
from sklearn.metrics import silhouette_score
import numpy as np
import csv
import ast
json_path = "ch02-test-quark"



def flatten_json(df):
    flat_dict = {}
    flat_dict['md5'] = df.iloc[0]['md5']
    flat_dict['apk_filename'] = df.iloc[0]['apk_filename']
    flat_dict['size_bytes'] = df.iloc[0]['size_bytes']
    flat_dict['threat_level'] = df.iloc[0]['threat_level']
    flat_dict['total_score'] = df.iloc[0]['total_score']
    for index, row in df.iterrows():        
        # Iterate over crimes list and flatten each crime into separate columns
        column_name_prefix = row['crimes']['rule']
        for key, value in row['crimes'].items():
            if key == 'rule':
                column_name_prefix = f"rule_{value}_"
            elif key in ['score', 'weight', 'confidence']:
                if key == 'confidence' and isinstance(value, str) and value.endswith('%'):
                    value = float(value.strip('%')) / 100.0  # Convert confidence to a float between 0 and 1
                flat_dict[column_name_prefix + key] = value
                flat_dict[column_name_prefix + key] = value

    return flat_dict

def process_folder(folder_path):

    features = []
    filenames = []
    folder_list = sorted(os.listdir(folder_path))
    combined_df = pd.DataFrame()
    for file in folder_list:

        file_path = folder_path + "/" + file
        df = pd.read_json(file_path)
        
        filenames.append(file)
        # Flatten JSON to DataFrame
        df_flattened = flatten_json(df)
        features.append(df_flattened)


# Create DataFrame
    combined_df = pd.DataFrame(features)
    return filenames, combined_df


filenames, df_combined = process_folder(json_path)


label_encoder = LabelEncoder()
df_combined['threat_level'] = label_encoder.fit_transform(df_combined['threat_level'])
# Preprocess the data (handle non-numeric columns, missing values, and normalization)
df_combined = df_combined.select_dtypes(include=[float, int]).fillna(0)
df_combined = df_combined.loc[:, df_combined.nunique() > 1]



excel_output_file = 'output_unscaled.xlsx'
df_combined.to_excel(excel_output_file, index=False)

# Normalize the data
scaler = StandardScaler()
df_scaled = scaler.fit_transform(df_combined)
excel_output_file = 'output_scaled.xlsx'
df_scaled_df = pd.DataFrame(df_scaled, columns=df_combined.columns)

df_scaled_df.to_excel(excel_output_file, index=False)

# Apply PCA to reduce dimensionality
# pca = PCA(n_components=0.95, random_state=42)  # Adjust n_components as needed
# df_pca = pca.fit_transform(df_scaled)
# excel_output_file = 'output_scaled_pca.xlsx'
# df_pca_df = pd.DataFrame(df_scaled, columns=df_combined.columns)
# df_pca_df.to_excel(excel_output_file, index=False)



# Define a range of hyperparameters to search over
eps_range = np.linspace( 0.1, 4.0, 10)
min_samples_range = range(3, 10)

best_score = -1
best_params = {'eps': None, 'min_samples': None}

# Iterate over hyperparameter combinations
# for eps in eps_range:
#     for min_samples in min_samples_range:
#         dbscan = DBSCAN(eps=eps, min_samples=min_samples)
#         labels = dbscan.fit_predict(df_scaled)
        
#         # Compute silhouette score (or another suitable metric) to evaluate clustering quality
#         score = silhouette_score(df_scaled, labels)
        
#         # Update best hyperparameters if current score is better
#         if score > best_score:
#             best_score = score
#             best_params['eps'] = eps
#             best_params['min_samples'] = min_samples

# print(f"Best Parameters: {best_params}")
# print(f"Best Silhouette Score: {best_score}")

# dbscan = DBSCAN(eps=best_params['eps'], min_samples=best_params['min_samples'])
# labels = dbscan.fit_predict(df_scaled)
# # Add cluster labels to the original DataFrame
# df_combined['cluster'] = dbscan.labels_

min_samples_range = range(3,20,1 )
# Perform clustering
best_score = -1
best_params = {'clusters': None}

for min_samples in min_samples_range:
        kmeans = KMeans(n_clusters=min_samples, random_state=42)
        labels = kmeans.fit_predict(df_scaled)

                # Compute silhouette score (or another suitable metric) to evaluate clustering quality
        score = silhouette_score(df_scaled, labels)
        
        # Update best hyperparameters if current score is better
        if score > best_score:
            best_score = score
            best_params['clusters'] = min_samples

print(f"Best Parameters: {best_params}")
print(f"Best Silhouette Score: {best_score}")

kmeans = KMeans(n_clusters=best_params['clusters'], random_state=42)
labels = kmeans.fit_predict(df_scaled)
# Add cluster labels to the original DataFrame
df_combined['cluster'] = kmeans.labels_
# Display the DataFrame with clusters



output_file = 'output3.csv'

with open(output_file, 'w', newline='') as csvfile:
        csvwriter = csv.writer(csvfile)
        for i in range(len(df_combined['cluster'])):
            csvwriter.writerow([filenames[i][:-5], df_combined['cluster'][i]])