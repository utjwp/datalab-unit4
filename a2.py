from sklearn.cluster import DBSCAN, KMeans
import os
import numpy as np
from sklearn.model_selection import train_test_split
from sklearn.ensemble import RandomForestClassifier
from sklearn.metrics import classification_report, accuracy_score
import joblib
from androguard.misc import AnalyzeAPK
import re
from androguard.core.analysis import analysis
from androguard.core.analysis import analysis
import xml.etree.ElementTree as ET
import pandas as pd

from androguard.core.apk import APK
from androguard.core.axml import AXMLPrinter
import xml.etree.ElementTree as ET
import os
import pandas as pd

from sklearn.model_selection import train_test_split
from sklearn.ensemble import RandomForestClassifier
from sklearn.metrics import classification_report, accuracy_score
from sklearn.preprocessing import LabelEncoder
from sklearn.feature_extraction.text import TfidfVectorizer

import joblib
import csv




def extract_features_from_manifest(manifest_path):
    features = {}
    #try:
    with open(manifest_path, 'rb') as manifest:
        manifest_content = manifest.read()
        axml = AXMLPrinter(manifest_content)
        xml = ET.fromstring(axml.get_xml())
        
        # Extract package name
        features['package_name'] = xml.attrib.get('package')
        
        # Extract permissions
        permissions = xml.findall(".//uses-permission")
        features['permissions'] = [perm.attrib.get('{http://schemas.android.com/apk/res/android}name') for perm in permissions]
        
        # Extract activities
        activities = xml.findall(".//activity")
        features['activities'] = [activity.attrib.get('{http://schemas.android.com/apk/res/android}name') for activity in activities]
        
        # Extract services
        services = xml.findall(".//service")
        features['services'] = [service.attrib.get('{http://schemas.android.com/apk/res/android}name') for service in services]
        
        # Extract receivers
        receivers = xml.findall(".//receiver")
        features['receivers'] = [receiver.attrib.get('{http://schemas.android.com/apk/res/android}name') for receiver in receivers]
        
        # Extract providers
        providers = xml.findall(".//provider")
        features['providers'] = [provider.attrib.get('{http://schemas.android.com/apk/res/android}name') for provider in providers]
        
        # Extract main activity
        main_activity = xml.findall(".//activity/intent-filter/action[@{http://schemas.android.com/apk/res/android}name='android.intent.action.MAIN']")
        if main_activity:
            features['main_activity'] = main_activity[0].attrib.get('{http://schemas.android.com/apk/res/android}name')
        else:
            features['main_activity'] = None
        
        # Extract min SDK version
        uses_sdk = xml.find(".//uses-sdk")
        features['min_sdk_version'] = uses_sdk.attrib.get('{http://schemas.android.com/apk/res/android}minSdkVersion') if uses_sdk is not None else None
        
        # Extract target SDK version
        features['target_sdk_version'] = uses_sdk.attrib.get('{http://schemas.android.com/apk/res/android}targetSdkVersion') if uses_sdk is not None else None
        
    # except Exception as e:
    #     print(f"Error extracting features from manifest: {e}")

    return features

def process_folder(folder_path, test=False):
    features = []
    folder_list = sorted(os.listdir(folder_path))
    for folder in folder_list:
        if not test:
            label = int(folder[-1])
            feature_dict['label'] = label
        manifest = folder_path + "/" + folder + '/AndroidManifest.xml'
        feature_dict = extract_features_from_manifest(manifest)
        feature_dict['app_id'] = folder
        
        features.append(feature_dict)

    df = pd.DataFrame(features)
    
    return df


def preprocess_data(df):
    # Fill missing values
    df = df.fillna('missing')
    # Convert lists to strings for permissions, activities, services, receivers, and providers
    for col in ['permissions', 'activities', 'services', 'receivers', 'providers']:
        df[col] = df[col].apply(lambda x: ' '.join(eval(x)) if (isinstance(x, str) and not 'None' in x) else 'missing')
    
    # Convert all entries to strings for the following columns
    for col in ['package_name', 'main_activity', 'min_sdk_version', 'target_sdk_version']:
        df[col] = df[col].astype(str)

    
    le = LabelEncoder()
    df['package_name'] = le.fit_transform(df['package_name'])
    df['main_activity'] = le.fit_transform(df['main_activity'])
    df['min_sdk_version'] = le.fit_transform(df['min_sdk_version'])
    df['target_sdk_version'] = le.fit_transform(df['target_sdk_version'])
    
    # Vectorize text features
    vectorizer = TfidfVectorizer(max_features=1000)
    permissions_features = vectorizer.fit_transform(df['permissions']).toarray()
    activities_features = vectorizer.fit_transform(df['activities']).toarray()
    services_features = vectorizer.fit_transform(df['services']).toarray()
    receivers_features = vectorizer.fit_transform(df['receivers']).toarray()
    providers_features = vectorizer.fit_transform(df['providers']).toarray()

    # Combine all features into a single DataFrame
    features = pd.concat([
        pd.DataFrame(permissions_features, index=df.index),
        pd.DataFrame(activities_features, index=df.index),
        pd.DataFrame(services_features, index=df.index),
        pd.DataFrame(receivers_features, index=df.index),
        pd.DataFrame(providers_features, index=df.index),
        df[['package_name', 'main_activity', 'min_sdk_version', 'target_sdk_version']]
    ], axis=1)
    
    # Ensure all column names are strings
    features.columns = features.columns.astype(str)
    
    return features


def test(df):
    X = df
    # Load the model
    kmeans = KMeans(n_clusters=20, random_state=42)
    kmeans.fit(X)
    labels = kmeans.labels_
    # Make predictions
    return labels
    
# Example usage
test_folder_path = './ch02-test-unzip'
output_file = 'output2.csv'

#df_features_test = process_folder(test_folder_path, test=True)
#df_features_test.to_csv('android_malware_features_test.csv', index=False)

df_features_test = pd.read_csv('android_malware_features_test.csv')
features = preprocess_data(df_features_test)
labels = test(features)
names = df_features_test["app_id"]


with open(output_file, 'w', newline='') as csvfile:
        csvwriter = csv.writer(csvfile)
        for i in range(len(names)):
            csvwriter.writerow([names[i], labels[i]])
