import os
import random

def process_json_folder(folder_path):

    filenames = []
    folder_list = sorted(os.listdir(folder_path))
    for file in folder_list:


        filenames.append(file[:-5])
   
        
    return filenames


def process_normal_folder(folder_path):
     file_list = sorted(os.listdir(folder_path))
     return file_list


zip_files = process_normal_folder("ch02-test")
json_files = process_json_folder("ch02-test-quark")

print(f"zip files: {len(zip_files)}")
print(f"json files: {len(json_files)}")

for file in zip_files:
    if file not in json_files:
        print(f"{file},{random.choice(range(0, 14, 1))}")