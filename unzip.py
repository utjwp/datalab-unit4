import zipfile
import shutil
import os


    
folder_path = "./ch02-test"
for filename in sorted(os.listdir(folder_path)):
    try:
        with zipfile.ZipFile(folder_path + "/" + filename, 'r') as zip_ref:
            zip_ref.extractall("./ch02-test-unzip/" + filename)
    except Exception as e:
        print(f"{filename} is not a zip file: {e}")