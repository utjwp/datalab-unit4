import os
import numpy as np
from sklearn.model_selection import train_test_split
from sklearn.ensemble import RandomForestClassifier
from sklearn.metrics import classification_report, accuracy_score
import joblib
from androguard.misc import AnalyzeAPK
import re
from androguard.core.analysis import analysis
from androguard.core.analysis import analysis
import xml.etree.ElementTree as ET
import pandas as pd

from androguard.core.apk import APK
from androguard.core.axml import AXMLPrinter
import xml.etree.ElementTree as ET
import os
import pandas as pd

from sklearn.model_selection import train_test_split
from sklearn.ensemble import RandomForestClassifier
from sklearn.metrics import classification_report, accuracy_score
from sklearn.preprocessing import LabelEncoder
from sklearn.feature_extraction.text import TfidfVectorizer

import joblib
import csv



def extract_features_from_manifest(manifest_path):
    features = {}
    #try:
    with open(manifest_path, 'rb') as manifest:
        manifest_content = manifest.read()
        axml = AXMLPrinter(manifest_content)
        xml = ET.fromstring(axml.get_xml())
        
        # Extract package name
        features['package_name'] = xml.attrib.get('package')
        
        # Extract permissions
        permissions = xml.findall(".//uses-permission")
        features['permissions'] = [perm.attrib.get('{http://schemas.android.com/apk/res/android}name') for perm in permissions]
        
        # Extract activities
        activities = xml.findall(".//activity")
        features['activities'] = [activity.attrib.get('{http://schemas.android.com/apk/res/android}name') for activity in activities]
        
        # Extract services
        services = xml.findall(".//service")
        features['services'] = [service.attrib.get('{http://schemas.android.com/apk/res/android}name') for service in services]
        
        # Extract receivers
        receivers = xml.findall(".//receiver")
        features['receivers'] = [receiver.attrib.get('{http://schemas.android.com/apk/res/android}name') for receiver in receivers]
        
        # Extract providers
        providers = xml.findall(".//provider")
        features['providers'] = [provider.attrib.get('{http://schemas.android.com/apk/res/android}name') for provider in providers]
        
        # Extract main activity
        main_activity = xml.findall(".//activity/intent-filter/action[@{http://schemas.android.com/apk/res/android}name='android.intent.action.MAIN']")
        if main_activity:
            features['main_activity'] = main_activity[0].attrib.get('{http://schemas.android.com/apk/res/android}name')
        else:
            features['main_activity'] = None
        
        # Extract min SDK version
        uses_sdk = xml.find(".//uses-sdk")
        features['min_sdk_version'] = uses_sdk.attrib.get('{http://schemas.android.com/apk/res/android}minSdkVersion') if uses_sdk is not None else None
        
        # Extract target SDK version
        features['target_sdk_version'] = uses_sdk.attrib.get('{http://schemas.android.com/apk/res/android}targetSdkVersion') if uses_sdk is not None else None
        
    # except Exception as e:
    #     print(f"Error extracting features from manifest: {e}")

    return features

def process_folder(folder_path, test=False):
    features = []
    folder_list = sorted(os.listdir(folder_path))
    for folder in folder_list:
        if not test:
            label = int(folder[-1])
            feature_dict['label'] = label
        manifest = folder_path + "/" + folder + '/AndroidManifest.xml'
        feature_dict = extract_features_from_manifest(manifest)
        feature_dict['app_id'] = folder
        
        features.append(feature_dict)

    df = pd.DataFrame(features)
    
    return df




def preprocess_data(df, df_test, test=False):
    # Fill missing values
    df = df.fillna('missing')
    # Convert lists to strings for permissions, activities, services, receivers, and providers
    for col in ['permissions', 'activities', 'services', 'receivers', 'providers']:
        df[col] = df[col].apply(lambda x: ' '.join(eval(x)) if isinstance(x, str) else 'missing')
    
    # Convert all entries to strings for the following columns
    for col in ['package_name', 'main_activity', 'min_sdk_version', 'target_sdk_version']:
        df[col] = df[col].astype(str)

    df_test = df_test.fillna('missing')
    # Convert lists to strings for permissions, activities, services, receivers, and providers
    for col in ['permissions', 'activities', 'services', 'receivers', 'providers']:
        df_test[col] = df_test[col].apply(lambda x: ' '.join(eval(x)) if isinstance(x, str) else 'missing')
    
    # Convert all entries to strings for the following columns
    for col in ['package_name', 'main_activity', 'min_sdk_version', 'target_sdk_version']:
        df_test[col] = df_test[col].astype(str)    

    df_combined = pd.concat([df, df_test], ignore_index=True)
    len_df = len(df)
    len_df_test = len(df_test)

    le = LabelEncoder()
    df_combined['package_name'] = le.fit_transform(df_combined['package_name'])
    df_combined['main_activity'] = le.fit_transform(df_combined['main_activity'])
    df_combined['min_sdk_version'] = le.fit_transform(df_combined['min_sdk_version'])
    df_combined['target_sdk_version'] = le.fit_transform(df_combined['target_sdk_version'])
    
    # Vectorize text features
    vectorizer = TfidfVectorizer(max_features=1000)
    permissions_features = vectorizer.fit_transform(df_combined['permissions']).toarray()
    activities_features = vectorizer.fit_transform(df_combined['activities']).toarray()
    services_features = vectorizer.fit_transform(df_combined['services']).toarray()
    receivers_features = vectorizer.fit_transform(df_combined['receivers']).toarray()
    providers_features = vectorizer.fit_transform(df_combined['providers']).toarray()

    # Combine all features into a single DataFrame
    features_combined = pd.concat([
        pd.DataFrame(permissions_features, index=df_combined.index),
        pd.DataFrame(activities_features, index=df_combined.index),
        pd.DataFrame(services_features, index=df_combined.index),
        pd.DataFrame(receivers_features, index=df_combined.index),
        pd.DataFrame(providers_features, index=df_combined.index),
        df_combined[['package_name', 'main_activity', 'min_sdk_version', 'target_sdk_version']]
    ], axis=1)
    
    # Ensure all column names are strings
    features_combined.columns = features_combined.columns.astype(str)
    
    joblib.dump(le, 'le.pkl')
    joblib.dump(vectorizer, 'vect.pkl')

    features = features_combined.iloc[:len_df].reset_index(drop=True)
    features_test = features_combined.iloc[len_df:].reset_index(drop=True)

    return features, features_test, df['label']

def train(features, labels):
    
    # Preprocess the data
    X, y = features, labels
    # Split the data into training and testing sets
    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.3, random_state=42)

    # Train a RandomForestClassifier
    clf = RandomForestClassifier(n_estimators=100, random_state=42)
    clf.fit(X_train, y_train)

    # Make predictions
    y_pred = clf.predict(X_test)

    # Evaluate the model
    print("Accuracy:", accuracy_score(y_test, y_pred))
    print("Classification Report:\n", classification_report(y_test, y_pred))

    # Save the model
    joblib.dump(clf, 'android_malware_classifier.pkl')

    print("Model training complete. Saved to 'android_malware_classifier.pkl'")

def test(df):
    X = df

    # Load the model
    clf = joblib.load('android_malware_classifier.pkl')

    # Make predictions
    y_pred = clf.predict(X)
    return y_pred
    
# Example usage
train_folder_path = './ch01-train-unzip'
test_folder_path = './ch01-test-unzip'
output_file = 'output.csv'

# df_features = process_folder(train_folder_path)
# df_features.to_csv('android_malware_features.csv', index=False)
# print("Feature extraction complete. Saved to 'android_malware_features.csv'")
#df_features_test = process_folder(test_folder_path, test=True)
# df_features_test.to_csv('android_malware_features_test.csv', index=False)


df = pd.read_csv('android_malware_features.csv')
df_features_test = pd.read_csv('android_malware_features_test.csv')
features, features_test, labels = preprocess_data(df, df_features_test)
train(features, labels)

names = df_features_test["app_id"]
labels = test(features_test)

with open(output_file, 'w', newline='') as csvfile:
        csvwriter = csv.writer(csvfile)
        for i in range(len(names)):
            csvwriter.writerow([names[i], labels[i]])